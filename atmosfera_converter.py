#!/usr/bin/env python
from os.path import exists, realpath, dirname
from os.path import isfile, basename, splitext
from os import makedirs
import os
import json
import shutil

# work paths
exe_path = dirname(realpath(__file__))
work_path = os.getcwd()
temp_path = os.path.join(work_path, 'temp')
out_path = os.path.join(work_path, 'out')
data_path = os.path.join(exe_path, 'data')
definitions_path = os.path.join(data_path, 'definitions')
build_path = os.path.join(work_path, 'build')

# paths needed for atmosfera creation
assets_path = os.path.join(build_path, 'assets')
atmosfera_path = os.path.join(assets_path, 'atmosfera')
sounds_path = os.path.join(atmosfera_path, 'sounds')
# lang_path = os.path.join(atmosfera_path, 'lang')
music_path = os.path.join(sounds_path, 'music')
# ambient_path = os.path.join(sounds_path, 'ambient')

# ambience paths
ambience_path = os.path.join(temp_path, 'ambience_music')
ambience_path = os.path.join(ambience_path, 'music')


def copy_and_rename(src: str, dest: str, src_name: str, dest_name: str):
    shutil.copy(os.path.join(src, src_name), dest, follow_symlinks=False)
    os.rename(os.path.join(dest, src_name), os.path.join(dest, dest_name))
    return


def get_pack_format(version: str) -> int:
    global data_path

    jsonfile = open(os.path.join(data_path, 'pack_formats.json'), 'r')
    pack_formats = json.load(jsonfile)
    jsonfile.close()

    mcversion = (version).split('.')
    if mcversion[1] in pack_formats.keys():
        keys = list(pack_formats.get(mcversion[1]).keys())
        if len(keys) > 1:
            keys.sort()
            keys.reverse()
            for key in keys:
                if int(mcversion[2]) >= int(key):
                    return pack_formats[mcversion[1]][key]
        else:
            return pack_formats[mcversion[1]][keys[0]]
    return None


def create_mc_meta(archivename: str, pack_format: int):
    global build_path

    print('Generating pack.mcmeta')

    mcmeta = {
        'pack': {
            'pack_format': pack_format,
            'description': archivename + ' for Atmosfera'
        }
    }

    if not exists(build_path):
        makedirs(build_path)

    with open(os.path.join(build_path, 'pack.mcmeta'), 'w') as mcmeta_file:
        json.dump(mcmeta, mcmeta_file, indent=2)
    return


def get_typo_free_name(name: str):
    if name == 'mebioDarkHill':
        name = 'medbioDarkHill'
    return name

def convert(name: str, sane_name: str, src_dir: str, dest_dir: str) -> bool:
    from pydub import AudioSegment

    name = get_typo_free_name(name)
    src_name = '%s.%s' % (name, 'mp3')
    src_file = os.path.join(src_dir, src_name)
    dest_name = '%s.%s' % (sane_name, 'ogg')
    dest_file = os.path.join(dest_dir, dest_name)
    if not exists(src_file):
        src_name = '%s.%s' % (name, 'ogg')
        src_file = os.path.join(src_dir, src_name)
        if not exists(src_file):
            print('Cannot convert %s, file does not exist!' % src_name)
            return False
        print(src_name, ' exists, copying')
        copy_and_rename(src_dir, dest_dir, src_name, dest_name)
        return True
    if not exists(dest_file):
        print('  Converting %s to ogg' % src_name)
        audio_file = AudioSegment.from_file(src_file, format='mp3')
        audio_file.export(dest_file, format='ogg')
    return True


def gen_soundfiles(music_dict: dict, shared_music: list):
    global atmosfera_path

    print('Generating sounds.json and converting files')

    def add_part(c: str, sounds_dict: dict):
        import string
        global music_path, ambience_path

        dictionary = music_dict
        main_path = music_path
        shared = shared_music

        shared_path = os.path.join(main_path, 'shared')
        snd_def_path = os.path.join(main_path, 'definitions')

        if not exists(shared_path):
            makedirs(shared_path)
        if not exists(snd_def_path):
            makedirs(snd_def_path)
        if len(dictionary.keys()) > 0:
            template = 'atmosfera:%s/%s/%s'

            for key in dictionary.keys():
                entry = {
                    'category': c,
                }
                sounds = []
                newpath = os.path.join(main_path, key)

                if not exists(newpath):
                    makedirs(newpath)
                copy_and_rename(
                    definitions_path,
                    snd_def_path,
                    key + '.json',
                    '%s_%s.json' % (key, c)
                )
                definitionfile = '%s_%s.json' % (key, c)
                definitionfile = os.path.join(snd_def_path, definitionfile)
                definition = json.load(open(definitionfile, 'r'))

                with open(definitionfile, 'w') as jsonfile:
                    name = str(definition['sound'] + '_' + c)
                    definition.update({'sound': name})
                    json.dump(definition, jsonfile, indent=2)

                for sound in dictionary.get(key):
                    sane_name = ''
                    for letter in sound.lower():
                        if letter not in [' ', '-', '_', '+', '.']:
                            if letter in string.ascii_lowercase + '0123456789':
                                sane_name = sane_name + letter
                        else:
                            sane_name = sane_name + '_'
                    folder = key
                    dest_path = newpath
                    if sound in shared:
                        folder = 'shared'
                        dest_path = shared_path
                    if convert(
                        sound,
                        sane_name,
                        ambience_path,
                        dest_path
                    ):
                        sounds.append(template % (c, folder, sane_name))
                entry.update({'sounds': sounds})
                sounds_dict.update({key + '_' + c: entry})
        return sounds_dict

    if not exists(atmosfera_path):
        makedirs(atmosfera_path)

    sounds_dict = add_part('music', {})
    # sounds_dict = add_part('ambient', sounds_dict)

    with open(os.path.join(atmosfera_path, 'sounds.json'), 'w') as soundsfile:
        json.dump(sounds_dict, soundsfile, indent=2)
    return


def get_ambience_properties() -> dict:
    global temp_path, ambience_path

    print('Getting Ambience categories')

    def parse_line(line: str, trigger: str) -> (str, str):
        global music_path
        out = (line[len(trigger):]).replace('\n', '').split('=')
        key = out[0].replace('+', '_').lower()
        values = out[1].split(',')
        return str(key), values

    def get_path_to_ambience() -> str:
        for elem in os.listdir(temp_path):
            properties_path = os.path.join(temp_path, elem)
            if os.path.isdir(properties_path):
                properties_path = os.path.join(temp_path, elem)
                properties_path = os.path.join(properties_path, 'ambience_music')
                if exists(properties_path):
                    return properties_path
        return None

    properties_path = os.path.join(temp_path, 'ambience_music')
    if not exists(properties_path):
        properties_path = get_path_to_ambience()
        if properties_path is None:
            return None, None
        ambience_path = os.path.join(properties_path, 'music')
    properties_path = os.path.join(properties_path, 'ambience.properties')
    event = dict()
    biome = dict()
    primarytag = dict()
    secondarytag = dict()
    ignored = []
    jsonfile = open(os.path.join(data_path, 'biomes.json'), 'r')
    biome_table = json.load(jsonfile)
    jsonfile.close()

    with open(properties_path, 'r') as temp_file:
        key, trigger = str(), str()
        values = []
        for line in temp_file:
            if line.startswith('event.'):
                trigger = 'event.'
            elif line.startswith('biome.'):
                trigger = 'biome.'
            elif line.startswith('primarytag.'):
                trigger = 'primarytag.'
            elif line.startswith('secondarytag.'):
                trigger = 'secondarytag.'
            if trigger != '':
                key, values = parse_line(line, trigger)
                if key in biome_table['conversion'].keys():
                    if biome_table['conversion'][key] != key:
                        key = biome_table['conversion'][key]
                if key not in ignored:
                    ignored.append(key)
                if trigger == 'event.':
                    event.update({key: values})
                elif trigger == 'biome.':
                    biome.update({key: values})
                elif trigger == 'primarytag.':
                    primarytag.update({key: values})
                elif trigger == 'secondarytag.':
                    secondarytag.update({key: values})
                trigger = ''
    output = {
        'event': event,
        'biome': biome,
        'primarytag': primarytag,
        'secondarytag': secondarytag
    }
    return output, ignored


def get_aliases() -> list:
    global data_path

    jsonfile = open(os.path.join(data_path, 'definitions.json'), 'r')
    definitions = json.load(jsonfile)
    jsonfile.close()
    aliases = []
    for key in definitions.keys():
        if key != 'ignore':
            aliases.append([key] + definitions.get(key)['alias'])
        else:
            aliases.append([key] + definitions.get(key))
    return aliases


def categorize_music(ambience_properties: dict, ignored: list) -> dict:
    global data_path

    print('Categorizing sounds')

    def add_element(music_dict: dict, c: str, elem: str) -> dict:
        if c in music_dict.keys():
            arr = music_dict[c]
            if song not in arr:
                arr.append(song)
                music_dict[c] = arr
        else:
            music_dict[c] = [song]
        return music_dict

    def check_if_seen(song: str, seen: list, shared: list) -> (list, list):
        if song in seen:
            if song not in shared:
                shared.append(song)
        else:
            seen.append(song)
        return seen, shared

    music_dict = dict()
    aliases = get_aliases()
    jsonfile = open(os.path.join(data_path, 'biomes.json'), 'r')
    biome_table = json.load(jsonfile)
    jsonfile.close()
    shared, seen = [], []

    for p in ambience_properties.keys():
        for entry in ambience_properties[p].keys():
            for song in ambience_properties[p][entry]:
                if entry in biome_table['tags'].keys():
                    categories = biome_table['tags'][entry]
                    if entry in ignored:
                        ignored.remove(entry)
                    for c in categories:
                        seen, shared = check_if_seen(song, seen, shared)
                        music_dict = add_element(music_dict, c, song)
                else:
                    for alias in aliases:
                        if entry.replace('_', '') in alias:
                            if alias[0].startswith('ignore'):
                                continue
                            if entry in ignored:
                                ignored.remove(entry)
                            seen, shared = check_if_seen(song, seen, shared)
                            music_dict = add_element(music_dict, alias[0], song)
    print('Ignored:', ignored)
    return music_dict, shared


def cleanup():
    global temp_path, build_path

    print('Cleaning up')
    shutil.rmtree(temp_path)
    shutil.rmtree(build_path)
    print('Done')
    return


def extract(archivefile: str):
    import zipfile, py7zr
    global temp_path

    print('Extracting Ambience pack')
    if not exists(temp_path):
        makedirs(temp_path)
    if py7zr.is_7zfile(archivefile):
        py7zr.unpack_7zarchive(archivefile, temp_path)
    else:
        with zipfile.ZipFile(archivefile) as archive:
            archive.extractall(temp_path)
    for _, dirs, _ in os.walk(temp_path):
        for directory in dirs:
            lower_name = directory.lower()
            if lower_name != directory:
                old_path = os.path.join(temp_path, directory)
                new_path = os.path.join(temp_path, lower_name)
                os.rename(old_path, new_path)
    return


def archiving(name: str):
    import zipfile
    global build_path, out_path

    print('Archiving files')

    if not exists(out_path):
        makedirs(out_path)

    name = name.replace('+', '-')
    archive = os.path.join(out_path, name + '.zip')
    rel_path = ''
    handle = zipfile.ZipFile(archive, 'w', zipfile.ZIP_DEFLATED)
    for root, _, files in os.walk(build_path):
        for file in files:
            rel_path = os.path.relpath(os.path.join(root, file), build_path)
            handle.write(os.path.join(root, file), rel_path)
    handle.close()
    return


def main() -> bool:
    import argparse

    archivefile = str()
    pack_format = int()

    # Options Begin
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--input',
        help='path to ambience archive'
    )
    parser.add_argument(
        '-v',
        '--version',
        help='target version of minecraft',
        nargs='?',
        const='1.17',
        default='1.17',
        type=str
    )
    args = parser.parse_args()
    if args.version:
        pack_format = get_pack_format(args.version)
        if pack_format is None:
            print('Invalid version.')
            return False
    if args.input:
        archivefile = args.input
        if not isfile(archivefile):
            print('File is not valid!')
            return False
        extract(archivefile)
    else:
        print('No input given.')
        return False
    archivename = splitext(basename(archivefile))[0]
    print('Pack:', archivename)
    create_mc_meta(archivename, pack_format)
    # check if is properties or csv type
    properties, ignored = get_ambience_properties()
    if properties is None:
        print('Couldnt find properties.')
        return False
    music_dict, shared_music = categorize_music(properties, ignored)
    gen_soundfiles(music_dict, shared_music)
    archiving(archivename)
    return True


if __name__ == '__main__':
    if main():
        print('Successfull!')
    else:
        print('Failed!')
    cleanup()
    print('Exiting...')
