# About
This tool is written in Python 3. It converts [Ambience](https://www.curseforge.com/minecraft/mc-mods/ambience-music-mod) packs, to a pack that [Atmosfera](https://github.com/Haven-King/Atmosfera) can understand.
In this initial stage, its not guaranteed all packs created with Ambience will work.
# Dependencies
- pydub
# TODO
- Replace minecraft sound files if needed
- Add more categories
